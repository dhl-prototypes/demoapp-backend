const Company = require('./../models').Company;
const Address = require('./../models').Address;
const IsoCountry = require('./../models').IsoCountry;
const { to, ReE, ReS } = require('../services/util.service');


// Company
let company = async function (req, res, next) {
    let company_id, err, company;
    company_id = req.params.company_id;

    [err, company] = await to(Company.findOne({where:{id:company_id}}));
    if(err) return ReE(res, "err finding company");

    if(!company) return ReE(res, "Company not found with id: "+company_id);
    let user, users_array, users;
    user = req.user;
    [err, users] = await to(company.getUsers());

    users_array = users.map(obj=>String(obj.user));

    if(!users_array.includes(String(user._id))) return ReE(res, "User does not have permission to read app with id: "+app_id);

    req.company = company;
    next();
}
module.exports.company = company;

// Address
let address = async function (req, res, next) {
    let address_id, err, address;
    address_id = req.params.address_id;

    [err, address] = await to(Address.findOne(
        {
            where:{
                id:address_id
            },
            include: {
                model: IsoCountry,
                attributes: {
                    exclude: ['createdAt', 'updatedAt']
                }
            }
        }
        ));
    if(err) return ReE(res, "err finding address");

    if(!address) return ReE(res, "Address not found with id: "+address_id);
    // let user, users_array, users;
    // user = req.user;
    // [err, users] = await to(address.getUsers());
    //
    // users_array = users.map(obj=>String(obj.user));
    //
    // if(!users_array.includes(String(user._id))) return ReE(res, "User does not have permission to read app with id: "+app_id);

    req.address = address;
    next();
}
module.exports.address = address;


// Country
let iso_country = async function (req, res, next) {
    let iso_country_id, err, iso_country;
    iso_country_id = req.params.iso_country_id;

    [err, iso_country] = await to(IsoCountry.findOne({where:{id:iso_country_id}}));
    if(err) return ReE(res, "err finding iso_country");

    if(!iso_country) return ReE(res, "IsoCountry not found with id: "+iso_country_id);
    // let user, users_array, users;
    // user = req.user;
    // [err, users] = await to(iso_country.getUsers());
    //
    // users_array = users.map(obj=>String(obj.user));
    //
    // if(!users_array.includes(String(user._id))) return ReE(res, "User does not have permission to read app with id: "+app_id);

    req.iso_country = iso_country;
    next();
}
module.exports.iso_country = iso_country;
