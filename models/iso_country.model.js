const {TE, to}              = require('../services/util.service');

module.exports = (sequelize, DataTypes) => {
    var Model = sequelize.define('IsoCountry', {
        country_iso: DataTypes.STRING,
        country_name: DataTypes.STRING
    });

    // Model.associate = function(models){
    //     this.IsoCountry = this.belongsToMany(models.IsoCountry, {through: 'AddressIsoCountry'});
    // };

    Model.prototype.toWeb = function (pw) {
        let json = this.toJSON();
        return json;
    };

    return Model;
};
