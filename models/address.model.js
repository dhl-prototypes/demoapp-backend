const {TE, to}              = require('../services/util.service');

module.exports = (sequelize, DataTypes) => {
    var Model = sequelize.define('Address', {
        company: DataTypes.STRING,
        name: DataTypes.STRING,
        address1: DataTypes.STRING,
        address2: DataTypes.STRING,
        address3: DataTypes.STRING,
        postal: DataTypes.STRING,
        city: DataTypes.STRING,
        email: DataTypes.STRING,
        phone: DataTypes.STRING
    });

    Model.associate = function(models){
        this.IsoCountry = this.belongsTo(models.IsoCountry);
    };

    Model.prototype.toWeb = function (pw) {
        let json = this.toJSON();
        return json;
    };

    return Model;
};
