const {IsoCountry} = require('../models');
const {to, ReE, ReS} = require('../services/util.service');

const create = async function (req, res) {
    let err, iso_country;

    // let user = req.user;
    let iso_country_info = req.body;

    [err, iso_country] = await to(IsoCountry.create(iso_country_info));
    if (err) return ReE(res, err, 422);

    [err, iso_country] = await to(iso_country.save());
    if (err) return ReE(res, err, 422);

    let iso_country_json = iso_country.toWeb();

    return ReS(res, {iso_country: iso_country_json}, 201);
};
module.exports.create = create;

const getAll = async function (req, res) {
    // let user = req.user;
    let err, iso_countries;

    [err, iso_countries] = await to(IsoCountry.findAll());

    let iso_countries_json = [];
    for (let i in iso_countries) {
        let iso_country = iso_countries[i];
        // let users = iso_country.Users;
        let iso_country_info = iso_country.toWeb();
        // let users_info = [];
        // for (let i in users) {
        //     let user = users[i];
        //     // let user_info = user.toJSON();
        //     users_info.push({user: user.id});
        // }
        // iso_country_info.users = users_info;
        iso_countries_json.push(iso_country_info);
    }

    console.log('c t', iso_countries_json);
    return ReS(res, {iso_countries: iso_countries_json});
};
module.exports.getAll = getAll;

const get = function (req, res) {
    let iso_country = req.iso_country;

    return ReS(res, {iso_country: iso_country.toWeb()});
};
module.exports.get = get;

const update = async function (req, res) {
    let err, iso_country, data;
    iso_country = req.iso_country;
    data = req.body;
    iso_country.set(data);

    [err, iso_country] = await to(iso_country.save());
    if (err) {
        return ReE(res, err);
    }
    return ReS(res, {iso_country: iso_country.toWeb()});
};
module.exports.update = update;

const remove = async function (req, res) {
    let iso_country, err;
    iso_country = req.iso_country;

    [err, iso_country] = await to(iso_country.destroy());
    if (err) return ReE(res, 'error occured trying to delete the iso_country');

    return ReS(res, {message: 'Deleted IsoCountry'}, 204);
};
module.exports.remove = remove;
