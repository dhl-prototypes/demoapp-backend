const {Address} = require('../models');
const {IsoCountry} = require('../models');
const {to, ReE, ReS} = require('../services/util.service');

const create = async function (req, res) {
    let err, address;

    let address_info = req.body;
    // let iso_country = req.iso_country;

    [err, address] = await to(Address.create(address_info));
    if (err) return ReE(res, err, 422);

    [err, address] = await to(address.save());
    if (err) return ReE(res, err, 422);

    let address_json = address.toWeb();

    return ReS(res, {address: address_json}, 201);
};
module.exports.create = create;

const getAll = async function (req, res) {
    let err, addresses;

    [err, addresses] = await to(Address.findAll(
        {
            include: {
                model: IsoCountry,
                attributes: {
                    exclude: ['createdAt', 'updatedAt']
                }
            }
        }));

    let addresses_json = [];
    for (let i in addresses) {
        let address = addresses[i];
        let address_info = address.toWeb();
        addresses_json.push(address_info);
    }

    console.log('c t', addresses_json);
    return ReS(res, {addresses: addresses_json});
};
module.exports.getAll = getAll;

const get = function (req, res) {
    let address = req.address;
    return ReS(res, {address: address.toWeb()});
};
module.exports.get = get;

const update = async function (req, res) {
    let err, address, data;
    address = req.address;
    data = req.body;
    address.set(data);

    [err, address] = await to(address.save());
    if (err) {
        return ReE(res, err);
    }
    return ReS(res, {address: address.toWeb()});
};
module.exports.update = update;

const remove = async function (req, res) {
    let address, err;
    address = req.address;

    [err, address] = await to(address.destroy());
    if (err) return ReE(res, 'error occured trying to delete the address');

    return ReS(res, {message: 'Deleted Address'}, 204);
};
module.exports.remove = remove;
