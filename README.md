# Rest Api Node and Mysql

## Description
Based of the template provided at: https://medium.com/@brianalois/build-a-rest-api-for-node-mysql-2018-jwt-6957bcfc7ac9

##### Routing         : Express
##### ORM Database    : Sequelize
##### Authentication  : Passport, JWT

## Installation

#### Download Code | Clone the Repo

```
git clone {repo_name}
```

#### Install Node Modules
```
npm install
```

#### Errors installing bcrypt
The original bcrpyt module has issues installing on Windows. Install bcryptjs instead, and replace

```
const bcrypt = require('bcrypt');
```

with
 
```
const bcrypt = require('bcryptjs');
```

Be sure to look for references in node_modules as well (demoapp-backend\node_modules\bcrypt-promise\lib\index.js:7:16)
)

#### Create .env File
You will find a example.env file in the home directory. Paste the contents of that into a file named .env in the same directory. 
Fill in the variables to fit your application

#### Start Dev Server

```npm
npm start
```

#### Create Initial User

POST http://localhost:3000/v1/users
```json
{
    "first": "admin",
    "last": "test",
    "email": "noreply@dhl.com",
    "password": "********"
}
```

#### Login
POST http://localhost:3000/v1/users/login
```json
{
	"email": "noreply@dhl.com",
	"password": "********"
}
```

JWT is returned as "token"

```json
{
    "token": "Bearer eyJhbGci...",
    "user": {
        "id": 1,
        "first": "admin",
        "last": "test",
        "email": "noreply@dhl.com",
        "phone": null,
        "password": "$2a$10$MPvhrfLyyoerlfL9gDOoY.h6uPJWwSc2JAvg7H8P2a4PgA/C/FTcq",
        "createdAt": "2019-06-25T22:43:14.000Z",
        "updatedAt": "2019-06-25T22:43:14.000Z"
    },
    "success": true
}
```

#### Sample Request

##### Add an Address
POST http://localhost:3000/v1/addresses

```json
  {
    "headers": {
      "Content-Type": "application/json",
      "Authorization": "Bearer eyJhbG...",
    },
    
    "data": {
        "company": "test_company_2",
        "name": "test_name_2",
        "address1": "test_address1_2",
        "address2": "test_address2_2",
        "address3": "test_address3_2",
        "postal": "test_postal_2",
        "city": "test_city_2",
        "email": "test_email_2",
        "IsoCountryId": 1
    }
  }
```

##### Get all addresses
GET http://localhost:3000/v1/addresses

##### Get an address
GET http://localhost:3000/v1/addresses/1

