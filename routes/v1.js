const express 			= require('express');
const router 			= express.Router();

const UserController 	= require('../controllers/user.controller');
const CompanyController = require('../controllers/company.controller');
const IsoCountryController = require('../controllers/iso_country.controller');
const AddressController = require('../controllers/address.controller');
const HomeController 	= require('../controllers/home.controller');


const custom 	        = require('./../middleware/custom');

const passport      	= require('passport');
const path              = require('path');


require('./../middleware/passport')(passport)
/* GET home page. */
router.get('/', function(req, res, next) {
  res.json({status:"success", message:"Parcel Pending API", data:{"version_number":"v1.0.0"}})
});


// User routes
router.post(    '/users',           UserController.create);                                                    // C
router.get(     '/users',           passport.authenticate('jwt', {session:false}), UserController.get);        // R
router.put(     '/users',           passport.authenticate('jwt', {session:false}), UserController.update);     // U
router.delete(  '/users',           passport.authenticate('jwt', {session:false}), UserController.remove);     // D
router.post(    '/users/login',     UserController.login);


// Company Routes
router.post(    '/companies',             passport.authenticate('jwt', {session:false}), CompanyController.create);                  // C
router.get(     '/companies',             passport.authenticate('jwt', {session:false}), CompanyController.getAll);                  // R

router.get(     '/companies/:company_id', passport.authenticate('jwt', {session:false}), custom.company, CompanyController.get);     // R
router.put(     '/companies/:company_id', passport.authenticate('jwt', {session:false}), custom.company, CompanyController.update);  // U
router.delete(  '/companies/:company_id', passport.authenticate('jwt', {session:false}), custom.company, CompanyController.remove);  // D


// Address routes
router.post(    '/addresses',             passport.authenticate('jwt', {session:false}), AddressController.create);                  // C
router.get(     '/addresses',             passport.authenticate('jwt', {session:false}), AddressController.getAll);                  // R

router.get(     '/addresses/:address_id', passport.authenticate('jwt', {session:false}), custom.address, AddressController.get);     // R
router.put(     '/addresses/:address_id', passport.authenticate('jwt', {session:false}), custom.address, AddressController.update);  // U
router.delete(  '/addresses/:address_id', passport.authenticate('jwt', {session:false}), custom.address, AddressController.remove);  // D


// Country routes
router.post(    '/iso_countries',             passport.authenticate('jwt', {session:false}), IsoCountryController.create);                  // C
router.get(     '/iso_countries',             passport.authenticate('jwt', {session:false}), IsoCountryController.getAll);                  // R

router.get(     '/iso_countries/:iso_country_id', passport.authenticate('jwt', {session:false}), custom.iso_country, IsoCountryController.get);     // R
router.put(     '/iso_countries/:iso_country_id', passport.authenticate('jwt', {session:false}), custom.iso_country, IsoCountryController.update);  // U
router.delete(  '/iso_countries/:iso_country_id', passport.authenticate('jwt', {session:false}), custom.iso_country, IsoCountryController.remove);  // D


// Misc routes
router.get('/dash', passport.authenticate('jwt', {session:false}),HomeController.Dashboard)


//********* API DOCUMENTATION **********
router.use('/docs/api.json',            express.static(path.join(__dirname, '/../public/v1/documentation/api.json')));
router.use('/docs',                     express.static(path.join(__dirname, '/../public/v1/documentation/dist')));
module.exports = router;
