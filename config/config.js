'use strict';

// initiate environment variables
require('dotenv').config();

// Global reference
let CONFIG = {};

CONFIG.app          = process.env.APP   || 'dev';
CONFIG.port         = process.env.PORT  || '3000';

CONFIG.db_dialect   = process.env.DB_DIALECT    || 'mysql';
CONFIG.db_host      = process.env.DB_HOST       || 'localhost';
CONFIG.db_port      = process.env.DB_PORT       || '3306';
CONFIG.db_name      = process.env.DB_NAME       || 'actest';
CONFIG.db_user      = process.env.DB_USER       || 'acadmin';
CONFIG.db_password  = process.env.DB_PASSWORD   || 'DHLadmin@1';

// // Get pw through keyring
// // Must first install keyring using python
// // pip install keyring
// // then set password by running
// // > keyring set mysql username
// const { spawnSync } = require( 'child_process' ),
//     pw = spawnSync( 'keyring', [ 'get', 'mysql', CONFIG.db_user] );
// // console.log( `stderr: ${pw.stderr.toString()}` );
// // console.log( `stdout: ${pw.stdout.toString()}` );
// CONFIG.db_password = pw.stdout.toString();

CONFIG.jwt_encryption  = process.env.JWT_ENCRYPTION || 'jwt_please_change';
CONFIG.jwt_expiration  = process.env.JWT_EXPIRATION || '10000';

module.exports = CONFIG;
